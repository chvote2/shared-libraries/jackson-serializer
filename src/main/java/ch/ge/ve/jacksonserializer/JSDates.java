/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - jackson-serializer                                                                             -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.jacksonserializer;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

/**
 * A class containing a serializer and deserializer for Java 8 temporal {@link LocalDateTime}s initialized with the
 * common CHVote date format.
 * <p>
 * Dates are serialized using the following pattern: <code>JSDate['dd.MM.yyyy HH:mm:ss.SSS']'</code>
 * (see {@link DateTimeFormatter}).
 * </p>
 */
public final class JSDates {

  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      new DateTimeFormatterBuilder().appendPattern("'JSDate['dd.MM.yyyy HH:mm:ss.SSS']'")
                                    .toFormatter();

  /**
   * The common CHVote local date serializer.
   */
  public static final LocalDateTimeSerializer SERIALIZER = new LocalDateTimeSerializer(DATE_TIME_FORMATTER);

  /**
   * The common CHVote local date deserializer.
   */
  public static final LocalDateTimeDeserializer DESERIALIZER = new LocalDateTimeDeserializer(DATE_TIME_FORMATTER);

  /**
   * Hide utility class constructor.
   */
  private JSDates() {
    throw new AssertionError("This class is not meant to be instantiated");
  }
}
