# CHVote: Jackson serializer
[![pipeline status](https://gitlab.com/chvote2/shared-libraries/jackson-serializer/badges/master/pipeline.svg)](https://gitlab.com/chvote2/shared-libraries/jackson-serializer/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=jackson-serializer&metric=alert_status)](https://sonarcloud.io/dashboard?id=jackson-serializer)

This library contains a set of Jackson serializer and deserializer for the CHVote system.

# Usage

## Add to your project

Maven:
```xml
<dependency>
    <groupId>ch.ge.ve</groupId>
    <artifactId>jackson-serializer</artifactId>
    <version>1.0.1</version>
</dependency>
```

## Configuring an `ObjectMapper`

The following snippet shows how to configure an `ObjectMapper` with the serializer and deserializers in this library:
```java
SimpleModule module = new SimpleModule();

module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer());
module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer());
module.addSerializer(LocalDateTime.class, JSDates.SERIALIZER);
module.addDeserializer(LocalDateTime.class, JSDates.SERIALIZER);

ObjectMapper mapper = new ObjectMapper().registerModule(module);
```

# Building

## Pre-requisites

* JDK 8
* Maven

## Build steps

```bash
mvn clean install
```

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](https://gitlab.com/chvote2/shared-libraries/jackson-serializer/blob/master/LICENSE) 
license.
